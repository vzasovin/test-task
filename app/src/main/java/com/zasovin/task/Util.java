package com.zasovin.task;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by Vadim on 12/18/2015.
 */
class Util {

    static int convertDpInPixels(Context context, int dp){
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return dp * displayMetrics.densityDpi / 160;
    }

}
