package com.zasovin.task;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

class DividerItemDecoration extends ItemDecoration {

    private int mDividerHeight;
    private Paint mDividerPaint;

    private int mOffsets;
    private int mAdjustedOffsets;
    private boolean mToAdjustOffsets = false;

    @SuppressWarnings("unused")
    public DividerItemDecoration() {
        this(1, 0x8a000000);
    }

    public DividerItemDecoration(int dividerHeight, int color) {
        this(dividerHeight, color, dividerHeight, false);
    }

    public DividerItemDecoration(int dividerHeight, int color, int offsets, boolean adjustOffsets) {
        super();

        if(offsets < dividerHeight){
            throw new IllegalArgumentException("Offsets can not be less than divider height");
        }else if(adjustOffsets){
            mToAdjustOffsets = true;
            mAdjustedOffsets = (offsets - dividerHeight)/2;
        }

        mOffsets = offsets;
        mDividerHeight = dividerHeight;

        mDividerPaint = new Paint();
        mDividerPaint.setColor(color);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               State state) {

        LayoutManager layoutManager = parent.getLayoutManager();
        if(layoutManager == null){
            throw new RuntimeException("LayoutManager not found");
        }
        if(layoutManager.getPosition(view) != 0)
            outRect.top = mOffsets;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, State state) {

    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, State state) {
        int left = parent.getPaddingLeft();
        int right = left + parent.getWidth()-parent.getPaddingRight();

        for(int i = 0;i<parent.getChildCount();i++){

            View child = parent.getChildAt(i);

            int top;
            //try to place divider in the middle of the space between elements
            if(!mToAdjustOffsets)
                top = child.getBottom();
            else top = child.getBottom() + mAdjustedOffsets;
            int bottom = top + mDividerHeight;

            c.drawRect(left, top, right, bottom, mDividerPaint);
        }
    }

}
