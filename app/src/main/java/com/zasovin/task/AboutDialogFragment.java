package com.zasovin.task;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by Vadim on 12/17/2015.
 */
public class AboutDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new MaterialDialog.Builder(getActivity())
                .title(R.string.about)
                .content(R.string.about_content)
                .positiveText(R.string.ok)
                .build();
    }

    static AboutDialogFragment newInstance(){
        return new AboutDialogFragment();
    }
}
