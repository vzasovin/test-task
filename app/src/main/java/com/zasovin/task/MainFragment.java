package com.zasovin.task;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Vadim on 12/17/2015.
 */
public class MainFragment extends Fragment {

    private static final String ABOUT_DIALOG_TAG = "MAIN_FRAGMENT_ABOUT_DIALOG";

    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView)inflater.inflate(R.layout.fragment_main, container, false);

        // set up RecyclerView
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        @SuppressWarnings("deprecation")
        int dividerColor = getResources().getColor(R.color.colorPrimary);
        int dividerHeight = Util.convertDpInPixels(getContext(), 1);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(dividerHeight, dividerColor);
        mRecyclerView.addItemDecoration(itemDecoration);

        // set an empty adapter
        mRecyclerView.setAdapter(new ItemAdapter(new ArrayList<ImageInfo>()));

        return mRecyclerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ContentLoaderAsyncTask().execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.main_menu_refresh:
                //refresh records
                new ContentLoaderAsyncTask().execute();
                return true;
            case R.id.main_menu_about:
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Fragment dialogFragment = fragmentManager.findFragmentByTag(ABOUT_DIALOG_TAG);

                if(dialogFragment == null){
                    dialogFragment = AboutDialogFragment.newInstance();
                    ((DialogFragment)dialogFragment).show(fragmentManager, ABOUT_DIALOG_TAG);
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    static MainFragment newInstance(){
        return new MainFragment();
    }

    private class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder>{

        class ItemViewHolder extends RecyclerView.ViewHolder{

            public ItemViewHolder(View itemView) {
                super(itemView);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentActivity activity = getActivity();

                        if(activity instanceof OnItemSelectedCallback){
                            OnItemSelectedCallback callback = (OnItemSelectedCallback)activity;
                            callback.onItemSelected(mDataSet, getAdapterPosition());
                        }
                    }
                });
            }
        }

        private List<ImageInfo> mDataSet;

        ItemAdapter(List<ImageInfo> data){
            mDataSet = data;
        }

        @SuppressLint("PrivateResource")
        @SuppressWarnings("deprecation")
        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_layout, parent, false);
            return new ItemViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position) {
            ImageInfo item = mDataSet.get(position);
            ((TextView)holder.itemView).setText(item.getTitle());
        }

        @Override
        public int getItemCount() {
            return mDataSet.size();
        }

    }

    private class ContentLoaderAsyncTask extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;

            try {
                URL url = new URL("http://private-db05-jsontest111.apiary-mock.com/androids");
                urlConnection = (HttpURLConnection)url.openConnection();
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                return readJsonString(inputStream);
            } catch (IOException e){
                e.printStackTrace();
                return null;
            }finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }
        }

        private String readJsonString(InputStream inputStream){
            Scanner scanner = new Scanner(inputStream);
            StringBuilder stringBuilder = new StringBuilder();

            while (scanner.hasNextLine()){
                stringBuilder.append(scanner.nextLine()).append("\n");
            }

            return stringBuilder.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(result == null){
                return;
            }

            List<ImageInfo> imageInfoList = parseJsonString(result);

            // update RecyclerView's content
            ItemAdapter adapter = new ItemAdapter(imageInfoList);
            mRecyclerView.swapAdapter(adapter,true);

            // display first item. This does nothing if the current layout is not two pane
            FragmentActivity activity = getActivity();

            if(activity instanceof OnItemSelectedCallback){
                OnItemSelectedCallback callback = (OnItemSelectedCallback)activity;

                callback.onContentLoaded(imageInfoList.get(0));
            }
        }

        private List<ImageInfo> parseJsonString(String json){
            List<ImageInfo> imageInfoList = new ArrayList<>(30);

            try {
                JSONArray jsonArray = new JSONArray(json);

                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.optJSONObject(i);

                    if(jsonObject == null){
                        continue;
                    }

                    String title = jsonObject.getString("title");
                    String imageURL = jsonObject.getString("img");

                    imageInfoList.add(new ImageInfo(title, imageURL));
                }

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            return imageInfoList;
        }
    }

    interface OnItemSelectedCallback{

        void onItemSelected(List<ImageInfo> imageInfoList, int position);

        void onContentLoaded(ImageInfo imageInfo);
    }

}
