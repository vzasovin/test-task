package com.zasovin.task;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vadim on 12/18/2015.
 */
public class DetailActivity extends AppCompatActivity {

    static final String EXTRA_IMAGE_INFO_LIST = "DETAIL_ACTIVITY_IMAGE_INFO_LIST";
    static final String EXTRA_SELECTED_ITEM_POSITION = "DETAIL_ACTIVITY_SELECTED_ITEM_POSITION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        ArrayList<ImageInfo> imageInfoList = intent
                .getParcelableArrayListExtra(EXTRA_IMAGE_INFO_LIST);
        int selectedItem = intent.getIntExtra(EXTRA_SELECTED_ITEM_POSITION, 0);

        setContentView(R.layout.activity_detail);

        ViewPager viewPager = (ViewPager)findViewById(R.id.viewPager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        ImageFragmentPagerAdapter adapter = new ImageFragmentPagerAdapter
                (fragmentManager, imageInfoList);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(selectedItem);
    }

    private static class ImageFragmentPagerAdapter extends FragmentStatePagerAdapter{

        private List<ImageInfo> mDataSet;

        public ImageFragmentPagerAdapter(FragmentManager fm, List<ImageInfo> dataSet) {
            super(fm);
            mDataSet = dataSet;
        }

        @Override
        public Fragment getItem(int position) {
            ImageInfo imageInfo = mDataSet.get(position);
            return ImageFragment.newInstance(imageInfo);
        }

        @Override
        public int getCount() {
            return mDataSet.size();
        }
    }
}
