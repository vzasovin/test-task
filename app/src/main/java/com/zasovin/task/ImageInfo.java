package com.zasovin.task;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Vadim on 12/17/2015.
 */
final class ImageInfo implements Parcelable {

    private String mTitle;
    private String mImageURL;

    ImageInfo(String title, String imageURL){
        mTitle = title;
        mImageURL = imageURL;
    }

    private ImageInfo(Parcel in) {
        mTitle = in.readString();
        mImageURL = in.readString();
    }

    String getTitle(){
        return mTitle;
    }

    String getImageURL() {
        return mImageURL;
    }

    public static final Creator<ImageInfo> CREATOR = new Creator<ImageInfo>() {
        @Override
        public ImageInfo createFromParcel(Parcel in) {
            return new ImageInfo(in);
        }

        @Override
        public ImageInfo[] newArray(int size) {
            return new ImageInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mTitle);
        parcel.writeString(mImageURL);
    }
}
