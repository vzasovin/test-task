package com.zasovin.task;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vadim on 12/17/2015.
 */
public class MainActivity extends AbstractActivity implements MainFragment.OnItemSelectedCallback{

    private boolean mTwoPaneLayout;
    private int mCurrentPosition;

    @Override
    protected Fragment createFragment() {
        return MainFragment.newInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTwoPaneLayout = findViewById(R.id.detailFragmentContainer) != null;
    }

    @Override
    public void onItemSelected(List<ImageInfo> imageInfoList, int position) {
        if(!mTwoPaneLayout){
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailActivity.EXTRA_SELECTED_ITEM_POSITION, position);
            intent.putParcelableArrayListExtra(DetailActivity.EXTRA_IMAGE_INFO_LIST,
                    (ArrayList<ImageInfo>)imageInfoList);
            startActivity(intent);
        }else if(mCurrentPosition != position){
            FragmentManager fragmentManager = getSupportFragmentManager();
            ImageFragment fragment = ImageFragment.newInstance(imageInfoList.get(position));
            fragmentManager.beginTransaction().
                    replace(R.id.detailFragmentContainer, fragment).commit();

            mCurrentPosition = position;
        }
    }

    @Override
    public void onContentLoaded(ImageInfo imageInfo) {
        if(mTwoPaneLayout){
            FragmentManager fragmentManager = getSupportFragmentManager();
            ImageFragment fragment = ImageFragment.newInstance(imageInfo);
            fragmentManager.beginTransaction().add(R.id.detailFragmentContainer, fragment).commit();

            mCurrentPosition = 0;
        }
    }
}
