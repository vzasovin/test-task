package com.zasovin.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by Vadim on 12/18/2015.
 */
public class ImageFragment extends Fragment {

    private static final String EXTRA_IMAGE_INFO_KEY = "IMAGE_FRAGMENT_EXTRA_IMAGE_INFO";
    private static final String OUT_STATE_IMAGE_INFO_KEY = "IMAGE_FRAGMENT_OUT_STATE_IMAGE_INFO";

    private ImageInfo mImageInfo;
    private ImageView mImageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            mImageInfo = savedInstanceState.getParcelable(OUT_STATE_IMAGE_INFO_KEY);
        }else {
            mImageInfo = getArguments().getParcelable(EXTRA_IMAGE_INFO_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);

        TextView titleTextView = (TextView)view.findViewById(R.id.title);
        titleTextView.setText(mImageInfo.getTitle());

        mImageView = (ImageView)view.findViewById(R.id.image);

        Callback.EmptyCallback callback = new Callback.EmptyCallback(){
            @Override
            public void onError() {
                super.onError();
                adjustImageViewLayoutParams(false);
            }

            @Override
            public void onSuccess() {
                super.onSuccess();
                adjustImageViewLayoutParams(true);
            }
        };

        Picasso.with(getContext())
                .load(mImageInfo.getImageURL())
                .error(R.drawable.ic_error_red_48dp)
                .into(mImageView, callback);

        return view;
    }

    private void adjustImageViewLayoutParams(boolean matchParent){
        int mode = matchParent ? LayoutParams.MATCH_PARENT : LayoutParams.WRAP_CONTENT;

        LayoutParams layoutParams = (LayoutParams)mImageView.getLayoutParams();
        layoutParams.width = mode;
        layoutParams.height = mode;

        mImageView.setLayoutParams(layoutParams);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Picasso.with(getContext()).cancelRequest(mImageView);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(OUT_STATE_IMAGE_INFO_KEY, mImageInfo);
    }

    static ImageFragment newInstance(ImageInfo imageInfo){
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_IMAGE_INFO_KEY, imageInfo);
        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
